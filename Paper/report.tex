\documentclass[9pt,twocolumn,twoside]{pnas-report}

\templatetype{pnasresearcharticle}
\usepackage[]{algorithm2e}


\title{Modeling network dynamics with the expanding-shrinking model}

\author[a,1]{Toni K. Turk}
\author[a]{Žiga F. Gorišek}

\affil[a]{University of Ljubljana, Faculty of Computer and Information Science, Ve\v{c}na pot 113, SI-1000 Ljubljana, Slovenia}

\leadauthor{Author}

\authordeclaration{All authors contributed equally to this work.}
\correspondingauthor{\textsuperscript{1}To whom correspondence should be addressed. E-mail: tk3152@student.uni-lj.si.}

\begin{abstract}

    Networks are used to model various complex real-world systems, ranging from protein interaction networks, World Wide Web, power grid, social interactions, and many more. Networks are, therefore, an invaluable tool for their analysis. Over the years, several statistical and computational metrics to analyze them have been proposed and established; however, interpreting their meaning is a non-trivial task. Understanding how a particular property of a network affects the system's behavior is a fundamental problem of network science. Random graph models have proven to be excellent tools to address it, as they give us a framework to compare computed metrics against and reveal intricate properties of network structure. Designing intuitive and straightforward models is, therefore, of great importance. Thus far, most research on this topic has been focused on models in which nodes or edges are continuously being added during network evolution. However, real networks usually evolve in both a growing and a shrinking manner. Consider, for instance, the World Wide Web (WWW); new web pages are constantly added to the network as well as removed. In this paper, we present a model that aims to encapsulate both kinds of dynamics. Henceforth, we show that the model produces networks with properties closely resembling those of real-world networks.

\end{abstract}

\dates{The manuscript was compiled on \today}
\doi{\href{https://ucilnica.fri.uni-lj.si/course/view.php?id=183}{Introduction to Network Analysis} 2020/21}

\begin{document}

    \maketitle
    \thispagestyle{firststyle}
    \ifthenelse{\boolean{shortarticle}}{\ifthenelse{\boolean{singlecolumn}}{\abscontentformatted}{\abscontent}}{}

    \dropcap{N}{\bf etworks}, often also referred to as graphs, are often the most sensible way to represent systems that can naturally be modeled with nodes (also referred to as vertices) and connections between the nodes (or edges). To study such networks, random graph models turned out to be a great tool. The study of random models started with the influential work of Erdos and Renyi \cite{ER59}. Since their work, this branch of discrete mathematics and network science has seen an explosion of interest and research, giving us a wide variety of tools to analyze networks.

    Surprisingly in a way, networks modeling systems of entirely different nature exhibit several similar characteristics. The aforementioned indicates that some natural laws must be governing the formation of network structure. Scientists try to identify such laws by designing mathematical models with the hope of obtaining realistic networks. Thus far, researchers have primarily been interested in models, which grow the size of the network over time. One such model is the Barabasi-Albert model \cite{BA99} (also known as the preferential-attachment model), in which both the number of nodes and edges increase as a function of time. Recently, Naglić et al. \cite{NS19a} proposed a shrinking model called a war pact model. They argue that shrinking might be reasonable for real-world systems, namely those representing warfare, international trade, cryptocurrency exchange, and similar. On the other hand, network models incorporating growing and shrinking dynamics have not yet seen much attention. Nevertheless, most, if not all, real networks grow and shrink simultaneously.

    For this reason, we present a novel model called an \textit{expanding-shrinking} (ES) model. The model starts with an arbitrary network, and at each step of the building procedure, the model either expands the network or shrinks it. In the remainder of the paper, we bestow a detailed empirical analysis of networks generated with this model and show that they display the properties of real-world systems.


    \section*{Related work}

    A random graph model is a model in which some network characteristics are pre-defined, and in other aspects, it is entirely random. More precisely, random graphs are defined in terms of a family of networks. Probably the simplest model is the one in which the number of nodes and edges is fixed. One then uniformly selects $ m $ distinct pairs of nodes and connects them with an edge. In the literature, this model is often called an Erdos-Renyi (ER) random graph \cite{ER59}, though, to our knowledge, they are not the original authors of the model itself. The model is simple to describe and easy to derive analytical equations from, making it a superb tool for analyzing network structure. A generalization of the ER model is a model, which allows its user to choose a degree sequence or a degree distribution of the nodes arbitrarily. One well-known example is the configuration model \cite{NSW01}, which is considered one of the most important theoretical models in network science \cite{Newman2010}.

    A completely different class of models is based on mechanisms that people think govern the formation of real networks. The best-known example is the \textit{preferential-attachment} model for obtaining networks with power-law \cite{Newman_2005} degree distribution. In this regard, the two most famous works are the Barabasi-Albert \cite{BA99} and Price \cite{Pri76} models. Another significant model is the so-called Watts-Strogatz random model \cite{WS98}. Watts and Strogatz were interested primarily in obtaining short distances and high clustering in the network. Their work is very informative as they were able to show that for short distances to emerge in a network, only a few \textit{shortcuts} throughout the network are necessary. However, their model is not widely used for analyzing real-world networks. An interesting new idea of a shrinking graph was recently proposed by Naglić et al. \cite{NS19a}. The authors developed a so-called war pact model in which the number of nodes shrinks over time with the number of edges remaining constant. They show that shrinking models, which have not been given much attention yet in the literature, might also prove good tools for network analysis.

    \section*{Proposed model}

    The model commences with an arbitrarily configured network. For our experiments, we mostly used the Erdos-Renyi \cite{NS19a} network configuration as the initial seed. Nevertheless, based on our empirical analysis, we state that, under the premise that sufficient iterations of the building process have been performed, the particular choice of the initial network configuration does not affect the outcome (we do not provide any evidence of this in the paper; the reader is encouraged to verify the claim on their own).

    At each iteration of the building procedure, the model either shrinks the network or expands it. We realize the network shrinking dynamic by selecting two nodes at random and merge them, just as in the original war pact model \cite{NS19a}. On the other hand, we propose the following strategy for node expansion: select a node uniformly at random and split it into two new nodes, with both nodes acquiring all edges from the chosen node. Consequently, not only nodes grow over time but edges as well. Figures \ref{fig:shrink} and \ref{fig:expand1} can provide visual intuition of the building process. Other network expansion methods might be reasonable as well; however, the strategy just described feels the most intuitive to us.


    \begin{figure}
        \includegraphics[width=\linewidth]{data/shrink.png}
        \caption{Realisation of node shrinking.}
        \label{fig:shrink}
    \end{figure}


    \begin{figure}
        \includegraphics[width=\linewidth]{data/expand1.png}
        \caption{Realisation of graph expansion by splitting a node; the two new nodes inherit all edges from the original node.}
        \label{fig:expand1}
    \end{figure}


    We control the probability of network shrinking / expansion with a parameter $ \alpha $, $ 0 \leq \alpha \leq 1$. Setting $ \alpha $ to $ 1 $ produces the original war pact model. Overall our model is in all dependent on two parameters: $ \alpha $ and the \textit{number of iterations} (or \textit{time} in other words). Both parameters play a meaningful role in the structures of constructed networks; hence a thorough analysis of their influence ensues shortly.

    It is, however, often easier to specify the desired size of the final network. We are able to calculate the expected size $ n_e $ of a generated network, given initial seed size $ n_i$, probability $ \alpha $, and time $ t $, using equation \ref{eq:targetsize}:

    \begin{equation}
        \label{eq:targetsize}
        n_e = n_i + t(1-\alpha) - t\alpha.
    \end{equation}
    Solving for $ t $, we get:


    \[ n_e = n_i + t(1 - 2\alpha) \]
    \[ n_e - n_i =  t(1 - 2\alpha) \]

    \begin{equation}
        \label{eq:nodes}
        t  =  \frac{n_i - n_e}{2\alpha - 1}.
    \end{equation}

    \noindent Observe that equation \ref{eq:nodes} is not defined for $ \alpha = 0.5 $. This makes perfect sense as the number of nodes would not change during model evolution. Additionally, the following two constraints hold as well: $ T > I \implies \alpha < 0.5$ and $ T < I \implies \alpha > 0.5 $.

    Implementing the expanding-shrinking model is straightforward; we implemented the war pact model as specified in the original paper for the shrinking dynamics (refer to the paper for an overview of the algorithm and its pseudocode). On the other hand, to split a node, we only need to add a new node to the network and connect it all the neighbors of the target node (review Algorithm 1 for more details)

    \begin{algorithm}[H]
        \SetAlgoLined
        \KwIn{node $ n $, graph $ g $}
        \KwOut{Graph $ g $}

        ns = neighbours(g, n);

        new = add\_node(g);

        \For{node k in ns}{
            add\_edge(g, new, k);
        }
        \caption{Node splitting in the ES model}
    \end{algorithm}

    \noindent Considering everything, at each iteration, the algorithm either executes the war pact model (with probability $alpha$) or expands the network by splitting a randomly selected node (Algorithm 2).

    \begin{algorithm}[H]
        \SetAlgoLined
        \KwIn{graph $ g $, probability $\alpha$, time $ t $}
        \KwOut{graph $ g $}

        i = 0;

        \For{$ i < t $}{
            h = random(0.0, 1.0);

            \If{$ h \leq \alpha $}{
                a = random(nodes(g));

                b = random(nodes(g));

                merge\_nodes(g, a, b);
            }\Else{
                a = random(nodes(g));

                split\_node(g, a);
            }
            i += 1;
        }

        \caption{Expanding-shrinking model}
    \end{algorithm}

    \section*{Results}

    We now present a comprehensive analysis of the generated networks using the ES model. We study standard network statistics; namely number of nodes $ n $ and edges $ m $, average degree $ \langle k \rangle = \frac{2m}{n} $, the fraction of nodes in the largest connected component $ LCC $, the average transitivity \cite{WS98}, also known as clustering coefficient, defined as $ \langle C \rangle = \frac{1}{n} \sum_{i}^{n}C_i$ with clustering coefficient of a specific node defined as $ C_i = \frac{2t_i}{k_i(k_i-1)} $, where $ t_i $ is the number of triangles including node $ i $ and $ k_i > 1 $, average distances $ \langle d \rangle = \frac{2}{n(n - 1)}\sum_{i<j}^{}d_{ij} $, where $ d_{ij} $ is the number of edges in the shortest paths between nodes $ i $ and $ j $, BFS-based effective diameter \cite{LKF07}, and node degree mixing coefficient $ r $, defined as Pearson's correlation of the degrees of connected nodes.

    Figure \ref{fig:stat-ER} compiles the properties of generated networks with the (ER) random graph used as initial seed; number of nodes $ n $ in the network increases if $ \alpha < 0.5 $, decreases if $ \alpha > 0.5 $, and persists constant for $ \alpha = 0.5 $. This observation is in line with equation \ref{eq:targetsize} (see figure \ref{fig:predicted_sizes}). The number of edges $ m $ increases as well, except for $ \alpha = 1.0 $. This is again expected due to the way we split nodes. An important property of real-world networks is their clustering coefficient \cite{WS98}; we observe that the ES model generates networks with no or negligible clustering for $ \alpha \leq 0.5 $, and arbitrarily high transitivity for $ \alpha > 0.5 $. Therefore, we have to perform network shrinking more than 50\% of the time to achieve non-negligible clustering. Again we remark that network shrinking dynamics are responsible for this phenomena.


    \begin{figure*}
        \includegraphics[width=\linewidth]{data/stat-ER.png}
        \caption{Statistics and properties of networks generated with the ES model with Erdos-Renyi random graph initial seed configuration.}
        \label{fig:stat-ER}
    \end{figure*}


    \begin{figure}
        \includegraphics[width=\linewidth]{data/network_sizes_prediction.png}
        \caption{Theoretical sizes of networks calculated with equation \ref{eq:targetsize}}.
        \label{fig:predicted_sizes}
    \end{figure}


    The most compelling observation, however, already deeply studied by Watts and Strogatz \cite{WS98}, and also independently by Leskovec et al. \cite{LKF07}, are the shrinking distances in the network. Watts and Strogatz developed a simple yet not pragmatic random graph model which randomly revires a small number of edges in a ring lattice. On the other hand, Leskovec et al. developed two more realistic yet more complex models that capture distance shrinking dynamics. In contrast, we attest that short distances can emerge in a network from a completely different, arguably more realistic, dynamic, namely, the node shrinking.

    Furthermore, we make a similar claim to Watts' and Strogatz's, which is that only a small number of node shrinking has to be performed to achieve small distances. Notice that this observation mostly makes sense for $ \alpha  $-s lower than $ 0.5 $, for which the networks grow over time. Shrinking distances are intuitively expected to emerge in networks that, over time, shrink, which does not make them that attractive in this regard.

    We conclude this part of the analysis by asserting that the ES model does not produce networks with a meaningful node degree mixing coefficient.

    \subsection{Degree distributions}

    Another relevant characteristic to scrutinize in network science is network degree distribution. Figure \ref{fig:degree-distributions} summarises the distributions of networks generated with the ES model using a random graph initial seed with $ n_i = 50000 $.

    \begin{figure*}
        \includegraphics[width=\linewidth]{data/dd.png}
        \caption{Degree distributions of networks generated with the ES model using ER random graph as initial seed. }
        \label{fig:degree-distributions}
    \end{figure*}

    A couple of things are worth pointing out; first, we see an emergence of high-degree nodes (also called hubs), which are present in (most) real-world systems. Yielding hubs is a desired property of any random graph model which strives to model the real world. Second, we see that the number of hubs is significantly smaller than that of small-degree nodes, again a real-world property. On the other hand, our model does not seem to reproduce an actual power-law degree distribution; nevertheless, we argue that the distributions of generated networks are comparable to those of (some) real networks, especially for $ \alpha \leq 0.5 $. Note that no additional knowledge (like preferential attachment) is hard-coded into the building procedure itself.

    \subsection{Comparison of networks generated with the ES model}

    Figure \ref{fig:heatmap} shows how networks generated with the ES model differ from each other. We calculate their similarity using a recently proposed \textit{portrait divergence} \cite{BB19, BBSA08} method. As expected, networks generated with the same $ \alpha $ are similar to one another. We observe another interesting phenomenon; networks generated with two different $ \alpha $-s inside the range $ \alpha \leq \sim0.35$ also display similar structures (at least from the perspective of portrait divergence), whereas networks generated outside of these two ranges completely diverge and do so not display similar properties. We conclude that the network expansion dynamic seems to be responsible for producing similar networks.

    \section*{Conclusion}

    Random graph models are great tools for network analysis. Unfortunately, not much research has been focused on models that shrink and grow the network simultaneously throughout the building procedure. For this reason, we extend the recently introduced war pact model by developing a novel model which incorporates not just shrinking but the expanding dynamic as well. Furthermore, we present a thorough study of generated networks and their qualities. We show that our model produces networks that display several characteristics of real-world systems, namely the emergence of a giant connected component, shrinking distances, non-negligible transitivity (for $ \alpha > 0.5 $), and the emergence of high-degree nodes (hubs).

    More importantly, we accomplish the results just stated with a straightforward and intuitive generation process with (close to) none apriori knowledge embedded into it. Nevertheless, the model fails in some domains. Probably the main concern is that it does not generate networks with the desired power-law degree distribution. Likewise, the model does not produce networks with a meaningful node degree mixing.

    An important quality of a model, which we have not discussed yet, is whether it can generate structurally similar networks to real-world networks. For this purpose, we developed a simple grid search method that generates networks with different parameters and compares them to a real-world network using portrait divergence. As a preliminary work, we performed the comparison to an international trade network and obtained a very good score of $ 0.18 $ with $ \alpha = 0.7 $, $ n_i = 7500 $, and $ t = 3100 $. Note that the score is averaged through five repetitions only and might statistically not be very robust. A more intelligent method should be developed for future work instead of a basic grid search. A method that would better exploit our knowledge of the model and converge to optimal parameters much faster.

    We believe that random graph models consolidating both shrinking and expanding dynamics show excellent promise, so we hope to inspire more research on this topic in the future.


    \begin{figure}[H]
        \includegraphics[width=\linewidth]{data/heatmap.png}
        \caption{Heatmap displaying the similarity of generated networks calculated with portrait divergence of networks generated with different $ \alpha$-s }
        \label{fig:heatmap}
    \end{figure}

%    \acknow{The authors would like to thank prof. Lovro Šubelj for his guidance and assistance in developing the model, performing meaningful analysis, and manifesting exciting results.}

% \showacknow{}

\bibliography{bibliography}

\end{document}