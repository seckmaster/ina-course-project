\documentclass[9pt,twocolumn,twoside]{pnas-proposal}

\usepackage[]{algorithm2e}

\templatetype{pnasresearcharticle}

\title{Expanding-shrinking model: a novel approach to modeling network dynamics}

\author[a,1]{Toni K. Turk}
\author[a,b]{Žiga F. Gorišek}

\affil[a]{University of Ljubljana, Faculty of Computer and Information Science, Ve\v{c}na pot 113, SI-1000 Ljubljana, Slovenia}

\leadauthor{Author}

\authordeclaration{All authors contributed equally to this work.}
\correspondingauthor{\textsuperscript{1}To whom correspondence should be addressed. E-mail: tk3152@student.uni-lj.si.}

\begin{abstract}

Networks are used to model a wide variety of complex real-world systems, ranging from protein interaction networks, World Wide Web, power grid, social interactions, and many more. Networks are, therefore, an invaluable tool for their analysis. Over the years, several statistical and computational metrics to analyse them have been proposed and established; however, interpreting their meaning is a non-trivial task. Understanding how a particular property of a network affects the system's behaviour is a fundamental problem of network science. To address it, random graph models have proven to be excellent tools, as they give us a framework to compare computed metrics against and reveal intricate properties of network structure. Designing intuitive and straightforward models is, therefore, of great importance. A model can be either static or dynamic. In a static model, the number of nodes or edges is fixed, whereas, in a dynamic model, the number of nodes or edges evolves. Most research on this topic has been focused on models in which nodes or edges are continuously being added. However, real networks usually evolve in both growing and shrinking manner. Consider, for instance, the World Wide Web (WWW); new web pages are constantly added to the network as well as removed. In this paper, we present a model that aims to encapsulate both kinds of dynamics. Henceforth, we show that the model produces networks with properties closely resembling those of real-world networks. Furthermore, we argue how our model makes sense from, to name a couple, military and company acquisition perspectives.

\end{abstract}

\dates{The manuscript was compiled on \today}
\doi{\href{https://ucilnica.fri.uni-lj.si/course/view.php?id=183}{Introduction to Network Analysis} 2020/21}

\begin{document}

\maketitle
\thispagestyle{firststyle}
\ifthenelse{\boolean{shortarticle}}{\ifthenelse{\boolean{singlecolumn}}{\abscontentformatted}{\abscontent}}{}

\dropcap{N}{\bf etworks}, often also referred to as graphs, are often the most sensible way to represent systems that can naturally be modelled with nodes (also referred to as vertices) and connections between the nodes (or edges). To study such networks, random graph models turned out to be a great tool. The study of random models started with the influential work of Erdos and Renyi \cite{ER59}. Since their work, this branch of discrete mathematics and network science has seen an explosion of interest and research, giving us a wide variety of tools to analyse networks.

Surprisingly in a way, networks modelling systems of entirely different nature exhibit several similar characteristics. The aforementioned indicates that some natural laws must be governing the formation of network structure. Scientists try to identify such laws by designing mathematical models with the hope of obtaining realistic networks. Thus far, researchers have primarily been interested in models, which grow the size of the network over time. One such model is the Barabasi-Albert model \cite{BA99} (also known as the preferential-attachment model), in which both the number of nodes and edges increases as a function of time. Recently, Naglić et al. \cite{NS19a} proposed a shrinking model called a war pact model. They argue that shrinking might be reasonable for some real-world systems, namely those representing warfare, international trade, cryptocurrency exchange, and similar. On the other hand, network models incorporating both growing and shrinking dynamics have not yet seen much attention. Nevertheless, most, if not all, real networks grow and shrink simultaneously.

For this reason, we present a novel model called an \textit{expanding-shrinking} (ES) model. The model starts with an arbitrary network and at each step of the building procedure, it either expands the network or shrinks it. In the remainder of the paper we bestow a detailed empirical analysis of networks generated with this model and show that they display the properties of real-world systems.


\section*{Related work}

A random graph model is a model in which some characteristics of a network are pre-defined, and in other aspects, it is entirely random. More precisely, random graphs are defined in terms of a family of networks. Probably the simplest model is the one in which the number of nodes and edges is fixed. One then uniformly selects $m$ distinct pairs of nodes and connects them with an edge. In the literature this model is often called an Erdos-Renyi (ER) random graph \cite{ER59}, though, to our knowledge, they are not the original authors of the model itself. It is simple to describe and easy to derive analytical equations from, making it a superb tool for analysing network structure. A generalisation of the ER model is a model, which allows its user to choose a degree sequence or a degree distribution of the nodes arbitrarily. One well-known example is the configuration model \cite{NSW01}, which is considered one of the most important theoretical models in network science \cite{Newman2010}.

A completely different class of models is based on mechanisms that people think govern the formation of real networks. The best-known example is the \textit{preferential-attachment} model for obtaining networks with power-law \cite{Newman_2005} degree distribution. In this regard, the two most famous works are the Barabasi-Albert \cite{BA99}, and Price \cite{Pri76} models. Another significant model is the so-called Watts-Strogatz random model \cite{WS98}. Watts and Strogatz were interested primarily in obtaining short distances and high clustering in the network. Their work is very informative as they were able to show that for short distances to emerge in a network, only a few \textit{shortcuts} throughout the network are necessary. However, their model is not widely used for analysing real networks. An interesting new idea of a shrinking graph was recently proposed by Naglić et al. \cite{NS19a}. The authors developed a so-called war pact model in which the number of nodes shrinks over time with the number of edges remaining constant. They show that shrinking models, which have not been given much attention yet in the literature, might also prove good tools for network analysis.

TODO mention work of Leskovec

\section{Proposed model}

The model starts with an arbitrarily configured network. For our experiments we mostly used the \textit{perfect matching} network configuration \cite{NS19a} for the initial seed. However, in section todo we show that, under the assumption that enough iterations of the building process have been performed, the particular choice of the initial network configuration does not affect the final outcome.

At each iteration of the building process it either shrinks the network or expands it. Node shrinking is realised in the same manner as in the war pact model \cite{NS19a}. More precisely, during a shrinking step in the graph building procedure, we select two nodes completely at random and merge them. On the other hand, we propose the following approach for node expansion: select a node uniformly at random and split it into two new nodes with both nodes inheriting all edges from the original node. Thus, not only nodes grow over time but edges as well. Figures \ref{fig:shrink} and \ref{fig:expand1} can provide visual intuition of the building process.


\begin{figure}
    \includegraphics[width=\linewidth]{shrink.png}
    \caption{Realisation of node shrinking.}
    \label{fig:shrink}
\end{figure}


\begin{figure}
    \includegraphics[width=\linewidth]{expand1.png}
    \caption{Realisation of graph expansion by splitting a node; the two new nodes inherit all edges from the original node.}
    \label{fig:expand1}
\end{figure}


We control the probability of network shrinking / expansion with a parameter $ \alpha $, $ 0 \leq \alpha \leq 1$. Setting $ \alpha $ to $ 1 $ produces the original war pact model. Hence, our model is in all dependent on two parameters: $ \alpha $ and the \textit{number of iterations}. Both parameters play a significant role on the structures of produced networks (a detailed analysis follows shortly).

It is, however, sometimes easier to specify the desired size of the final network. We are able to calculate the expected size of the generated network $ T $, given initial seed size $ I $, $ \alpha $, and the number of iterations $ n $, using equation \ref{eq:targetsize}:

\begin{equation}
    \label{eq:targetsize}
    T = I + n(1-\alpha) - n\alpha,
\end{equation}

where $ T $ represents the target network size. Solving for $ n $, we get:


\[ T = I + n(1 - 2\alpha) \]
\[ T - I =  n(1 - 2\alpha) \]

\begin{equation}
    \label{eq:nodes}
    n  =  \frac{I - T}{2\alpha - 1}.
\end{equation}

Observe that equation \ref{eq:nodes} is not defined for $ \alpha = 0.5 $. This makes perfect sense as the number of nodes would not change during model evolution. Additionally, the following two constraints hold as well: $ T > I \implies \alpha < 0.5$ and $ T < I \implies \alpha > 0.5 $.

The implementation of the expanding-shrinking model is straightforward; we implemented the war pact model as specified in the original paper for the shrinking dynamics (refer to the paper for an overview of the algorithm and the pseudocode). On the other hand, to split a node we only need to add a new node to the network and connect it all the neighbours of the target node (review the Algorithm 1 for more details).


\begin{algorithm}[H]
    \SetAlgoLined
    \KwIn{node n, graph g}
    \KwOut{Graph g}

    ns = neighbours(g, n);

    new = add\_node(g);

    \For{node k in ns}{
        add\_edge(g, new, k);
    }
    \caption{Node splitting in the ES model}
\end{algorithm}

Taking everything into consideration, at each iteration the algorithm either executes the war pact model (with probability $alpha$), or expands the network by splitting a randomly selected node (Algorithm 2).

\begin{algorithm}[H]
    \SetAlgoLined
    \KwIn{graph g, probability $\alpha$, number of iterations iter}
    \KwOut{graph g}

   i = 0;

   \For{$ i < iter $}{
       h = random(0.0, 1.0);

       \If{h < $\alpha$}{
           a = random(nodes(g));

           b = random(nodes(g));

           merge\_nodes(g, a, b);
       }\Else{
           a = random(nodes(g));

           split\_node(g, a);
       }
        i += 1;
   }

    \caption{Expanding-shrinking model}
\end{algorithm}

\section{Network analysis}

We now present a comprehensive analysis of the effect that the two parameters have on generated networks.

\begin{figure}
    \includegraphics[width=\linewidth]{data/stat-PM.png}
    \caption{Network statistics.}
    \label{fig:stat-PM}
\end{figure}



\bibliography{bibliography}

\end{document}