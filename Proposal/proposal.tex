\documentclass[9pt,twocolumn,twoside]{pnas-proposal}

\templatetype{pnasresearcharticle}

\title{Expanding  - shrinking model: a novel approach to modeling network dynamics}

\author[a,1]{Toni K. Turk}
\author[a,b]{Žiga F. Gorišek}

\affil[a]{University of Ljubljana, Faculty of Computer and Information Science, Ve\v{c}na pot 113, SI-1000 Ljubljana, Slovenia}

\leadauthor{Author}

\authordeclaration{All authors contributed equally to this work.}
\correspondingauthor{\textsuperscript{1}To whom correspondence should be addressed. E-mail: tk3152@student.uni-lj.si.}

\begin{abstract}

Networks are used to model a wide variety of complex real-world systems, ranging from protein interaction networks, World Wide Web, power grid, social interactions, and many more. Networks are, therefore, an invaluable tool for their analysis. Over the years, several statistical and computational metrics to analyse them have been proposed and established; however, interpreting their meaning is a non-trivial task. Understanding how a particular property of a network affects the system's behaviour is a fundamental problem of network science. To address it, random graph models have proven to be excellent tools, as they give us a framework to compare computed metrics against and reveal intricate properties of network structure. Designing intuitive and straightforward models is, therefore, of great importance. A model can be either static or dynamic. In a static model, the number of nodes or edges is fixed, whereas, in a dynamic model, the number of nodes or edges evolves. Most research on this topic has been focused on models in which nodes or edges are continuously being added. However, real networks usually evolve in both growing and shrinking manner. Consider, for instance, the World Wide Web (WWW); new web pages are constantly added to the network as well as removed. In this paper, we present a model that aims to encapsulate both kinds of dynamics. Henceforth, we show that the model produces networks with properties closely resembling those of real-world networks. Furthermore, we argue how our model makes sense from, to name a couple, military and company acquisition perspectives.

\end{abstract}

\dates{The manuscript was compiled on \today}
\doi{\href{https://ucilnica.fri.uni-lj.si/course/view.php?id=183}{Introduction to Network Analysis} 2020/21}

\begin{document}

\maketitle
\thispagestyle{firststyle}
\ifthenelse{\boolean{shortarticle}}{\ifthenelse{\boolean{singlecolumn}}{\abscontentformatted}{\abscontent}}{}

\dropcap{N}{\bf etworks}, often also referred to as graphs, are often the most sensible way to represent systems that can naturally be modelled with nodes (also referred to as vertices) and connections between the nodes (or edges). To study such networks, random graph models turned out to be a great tool. The study of random models started with the influential work of Erdos and Renyi \cite{ER59}. Since their work, this branch of discrete mathematics and network science has seen an explosion of interest and research, giving us a wide variety of tools to analyse networks.

Surprisingly in a way, networks modelling systems of entirely different nature exhibit several similar characteristics. The aforementioned indicates that some natural laws must be governing the formation of network structure. Scientists try to identify such laws by designing mathematical models with the hope of obtaining realistic networks. Thus far, researchers have primarily been interested in models, which grow the size of the network over time. One such model is the Barabasi-Albert model \cite{BA99} (also known as the preferential-attachment model), in which both the number of nodes and edges increases as a function of time. Recently, Naglić et al. \cite{NS19a} proposed a shrinking model called a war pact model. They argue that shrinking might be reasonable for some real-world systems, namely those representing warfare, international trade, cryptocurrency exchange, and similar. On the other hand, network models incorporating both growing and shrinking dynamics have not yet seen much attention. Nevertheless, most, if not all, real networks grow and shrink simultaneously.

For this reason, we present a novel model called an \textit{expanding - shrinking} (ES) model. The model starts with some number of nodes $n$ and zero edges. At each step of the building procedure, it either expands the network or shrinks it. In the remainder of the paper we bestow a detailed empirical analysis of networks generated with this model and show that they display the properties of real-world systems.


\section*{Related work}

A random graph model is a model in which some characteristics of a network are pre-defined, and in other aspects, it is entirely random. More precisely, random graphs are defined in terms of a family of networks. Probably the simplest model is the one in which the number of nodes and edges is fixed. One then uniformly selects $m$ distinct pairs of nodes and connects them with an edge. In the literature this model is often called an Erdos-Renyi (ER) random graph \cite{ER59}, though, to our knowledge, they are not the original authors of the model itself. It is simple to describe and easy to derive analytical equations from, making it a superb tool for analysing network structure. A generalisation of the ER model is a model, which allows its user to choose a degree sequence or a degree distribution of the nodes arbitrarily. One well-known example is the configuration model \cite{NSW01}, which is considered one of the most important theoretical models in network science \cite{Newman2010}.

A completely different class of models is based on mechanisms that people think govern the formation of real networks. The best-known example is the \textit{preferential-attachment} model for obtaining networks with power-law \cite{Newman_2005} degree distribution. In this regard, the two most famous works are the Barabasi-Albert \cite{BA99}, and Price \cite{Pri76} models. Another significant model is the so-called Watts-Strogatz random model \cite{WS98}. Watts and Strogatz were interested primarily in obtaining short distances and high clustering in the network. Their work is very informative as they were able to show that for short distances to emerge in a network, only a few \textit{shortcuts} throughout the network are necessary. However, their model is not widely used for analysing real networks. An interesting new idea of a shrinking graph was recently proposed by Naglić et al. \cite{NS19a}. The authors developed a so-called war pact model in which the number of nodes shrinks over time with the number of edges remaining constant. They show that shrinking models, which have not been given much attention yet in the literature, might also prove good tools for network analysis.

\section*{Project proposal}\label{proposal}

\subsection{Motivation}

Our paper aims to propose a new dynamic random graph model, which will be based on node shrinking and expanding dynamics. Thus far, the literature on graph models has been chiefly focused on expanding graph models, in which nodes are added to the network over time but never removed. Just recently a model based on node shrinking has been proposed. We believe, however, that the real world behaves in both manners simultaneously. Thus, we suspect that we can better reflect this kind of systems with a model that incorporates both shrinking and expanding.

\subsection{Core ideas}\label{ideas}

We have a couple of ideas that are the basis of our effort;

The shrinking dynamics of network generation will be based on the war pact model. More precisely, during a shrinking step in the graph building procedure, we will select two nodes at random or preferentially in some way and merge them. The shrinking of nodes in the context of real networks might represent the following; in a war effort network, the merging of two nodes represents the formation of an alliance, and in the context of a company cooperation network, the merging represents the merger of the two companies.

On the other hand, the expanding dynamics can be achieved in several ways, though we are primarily interested in two. Firstly, we can use the approach of the Barabasi-Albert model, in which new nodes are added one by one and attached to the network with preferential attachment. This makes perfect sense; however, we believe that we can do better. Instead of looking only at the degree of the nodes, we suspect that using a centrality that examines the bigger picture might yield more sensible results. From different perspectives, eigenvector, reciprocal eigenvector, and PageRank centralities seem promising.

Secondly, instead of adding new nodes to the network, we can split existing nodes. Thus, the number of nodes still grows by one node in each iteration. This makes sense in several ways. For instance, alliances between countries in times of war can fall apart, and companies can split. When a node is split, the two new nodes can either inherit all of the edges of the original node or half of them each (other dividing strategies might be possible as well, though these two seem the most sensible to us at this moment). Figures \ref{fig:shrink} - \ref{fig:expand3} can perhaps provide additional intuition of the building process.

Our model is expected to have one parameter $\alpha$, which will represent the probability of performing either shrinking or network expansion (as described above). Note that $0 \leq \alpha \leq 1$. When alpha equals $ 0.5 $, both events have an equal chance of happening, so the number of nodes is expected to remain constant. If $\alpha$ is set to $0$, we obtain the original war pact model.

Instead of analysing the network structure itself when selecting nodes and edges during network construction, one might instead assign to nodes some \textit{value}. One way of interpreting this value would be, in the war context, as a \textit{goal} of a country. Therefore, a sensible way to form alliances would be to select a country with the most \textit{similar} goal, and when looking for an enemy, a country with the most \textit{different} goal.


\begin{figure}
    \includegraphics[width=\linewidth]{shrink.png}
    \caption{Realisation of node shrinking.}
    \label{fig:shrink}
\end{figure}


\begin{figure}
    \includegraphics[width=\linewidth]{expand1.png}
    \caption{Realisation of graph expansion by splitting a node; the two new nodes inherit all edges from the original node.}
    \label{fig:expand1}
\end{figure}


\begin{figure}
    \includegraphics[width=\linewidth]{expand2.png}
    \caption{Realisation of graph expansion by splitting a node; the two new nodes each inherit half of edges from the original node. Note that splitting a node with a single edge is currently undefined. }
    \label{fig:expand2}
\end{figure}


\begin{figure}
    \includegraphics[width=\linewidth]{expand3.png}
    \caption{Realisation of graph expansion by preferential attachment. A single edge is added between the two selected nodes.}
    \label{fig:expand3}
\end{figure}

\subsection{Expected contributions}

Random graph models are an essential tool in the arsenal of a network scientist, as they can be used to reason about network structure and its dynamics. Consequently, it is crucial to develop models that are easy to understand and are reality-based. The expected contribution of this paper is a new kind of model, which, as we believe, could be a better approximation of network growth and evolution. Furthermore, we hope to inspire other scientists to continue the research of such models in the future.

\subsection{Methods and algorithms}

We will perform a detailed analysis of networks obtained with our model. Namely, we will analyse degree distributions, clustering, distances, node mixing, size of the largest connected component, and potentially some other relevant metrics. We will contrast computed properties with properties of some real networks representing systems our model aims to represent. Furthermore, we will compare the networks using recently proposed information-theoretic measures such as D-measure \cite{SCDPMR17} or portrait divergence \cite{BB19, BBSA08}, to obtain a rigorous comparison. Most importantly, we will give an empirical analysis, and hopefully, some intuition, of the implication the parameter $\alpha$ has on the construction of networks with our model.

\section*{Preliminary work}

Our work so far has been mainly focused on developing ideas, thinking about what makes sense. On the other hand, no \textit{concrete} work has been done to develop the models and perform experiments.

\bibliography{bibliography}

\end{document}