#ifndef HW1_ARG_PARSER_H
#define HW1_ARG_PARSER_H

#include <optional>
#include <string>
#include <sstream>

std::optional<std::string> parse_arg(const std::string& args,
                                     const std::string& arg_name) {
    auto index = args.find(arg_name);
    if (index == std::string::npos) {
        return {};
    }
    auto start_index = index + arg_name.size() + 1;
    if (start_index >= args.size()) {
        return {};
    }
    auto end_index = args.find(' ', start_index);
    if (end_index == std::string::npos) {
        end_index = args.size();
    }
    auto arg_value = args.substr(start_index, end_index - start_index);
    return arg_value;
}

std::optional<double> parse_double(const std::string& args,
                                   const std::string& arg_name) {
    auto optional_arg_value = parse_arg(args, arg_name);
    if (optional_arg_value) {
        try {
            auto double_value = std::stod(*optional_arg_value);
            return double_value;
        } catch (const std::invalid_argument&) {
            return {};
        } catch (const std::out_of_range&) {
            return {};
        }
    }
    return {};
}

std::optional<int> parse_int(const std::string& args,
                             const std::string& arg_name) {
    auto optional_arg_value = parse_arg(args, arg_name);
    if (optional_arg_value) {
        try {
            auto int_value = std::stoi(*optional_arg_value);
            return int_value;
        } catch (const std::invalid_argument&) {
            return {};
        } catch (const std::out_of_range&) {
            return {};
        }
    }
    return {};
}

std::optional<bool> parse_bool(const std::string& args,
                               const std::string& arg_name) {
    auto optional_arg_value = parse_arg(args, arg_name);
    if (optional_arg_value) {
        if (*optional_arg_value == "1" || *optional_arg_value == "true") {
            return true;
        }
        if (*optional_arg_value == "0" || *optional_arg_value == "false") {
            return false;
        }
    }
    return {};
}

std::string to_string(char **argv, int argc) {
    std::stringstream ss;
    for (int i = 0; i < argc; i++) {
        ss << argv[i] << " ";
    }
    return ss.str();
}

#endif //HW1_ARG_PARSER_H
