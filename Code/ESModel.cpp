#include "ESModel.h"
#include "ProgressBar.h"

#include <functional>

using namespace std;

void expanding_shrinking_model(float alpha,
                               int iter,
                               int seed,
                               const function<pair<int, int>()> &select_nodes_for_merging,
                               const function<int()> &select_node_for_splitting,
                               const function<void(unsigned, unsigned)> &merge_nodes,
                               const function<void(unsigned)> &split_node,
                               const function<int()> &size,
                               bool verbose) {
    assert(alpha <= 1.0);
    assert(alpha >= 0.0);

    srand(seed);

    for (int i = 0; i < iter; i++) {
        auto rnd = (float) rand() / (RAND_MAX);
        if (rnd < alpha) {
            /// shrink
            if (size() <= 1) { continue; }
            auto nodes = select_nodes_for_merging();
            merge_nodes(nodes.first, nodes.second);
        } else {
            /// expand
            auto node = select_node_for_splitting();
            split_node(node);
        }            
        if (verbose) { progress_bar(i + 1, iter); }
    }
    if (verbose) { cout << endl; }
}

void expanding_shrinking_model(Graph &g,
                               float alpha,
                               int iter,
                               int seed,
                               bool verbose) {
    expanding_shrinking_model(alpha, iter, seed,
                             [&g]() { /// random selection
                                int a;
                                int b;
                                do {
                                    a = rand() % g.get_nodes_count();
                                    b = rand() % g.get_nodes_count();
                                } while (a == b);
                                return make_pair(a, b);
                             },
                             [&g]() { /// random selection
                                 return rand() % g.get_nodes_count();
                             },
                             [&g](auto a, auto b) {
                                 merge_nodes(g, a, b);
                             },
                             [&g](auto a) {
                                 split_node(g, a);
                             },
                             [&g]() { 
                                 return g.get_nodes_count(); 
                             },
                             verbose);
}
