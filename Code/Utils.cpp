#include "Utils.h"
#include "ESModel.h"

using namespace std;

// template<class T>
// std::vector<T*> filter(const std::vector<T> &vec, std::function<bool(const T&)> predicate) {
//     auto filtered = vector<T>();
//     copy_if(vec.begin(), vec.end(), 
//             back_inserter(filtered), 
//             predicate);
//     return filtered;
// }

// template<typename T, typename U>
// vector<U> fmap(const vector<T> &vec, function<U(const T&)> map_to) {
//     auto mapped = vector<U>();
//     transform(vec.begin(), vec.end(), 
//               back_inserter(mapped), 
//               map_to);
//     return mapped;
// }

void merge_nodes(Graph &g, unsigned a, unsigned b) {
    assert(a < g.get_nodes_count());
    assert(b < g.get_nodes_count());
    assert(a != b);

    if (a > b) {
        swap(a, b);
    }

    auto neighbours = g.get_neighbours(b);
    g.remove_node(b);

    for (auto it = neighbours.cbegin(); it != neighbours.cend(); it++) {
        auto n = *it;
        if (n >= b) { 
            n--;
        }
        if (a == n || g.are_nodes_adjacent(a, n)) continue;
        g.add_edge(a, n);
    }
}

void split_node(Graph &g, unsigned a) {
    assert(a < g.get_nodes_count());

    auto neighbours = g.get_neighbours(a);
    g.add_node();
    auto new_node = g.get_nodes_count() - 1;

    for (auto it = neighbours.cbegin(); it != neighbours.cend(); it++) {
        g.add_edge(new_node, *it);
    }
}

shared_ptr<Graph> perfect_matching_network(int nodes) {
    assert(nodes % 2 == 0);

    auto g = make_shared<Graph>(nodes, false);
    auto half = nodes / 2;
    for (int i = 0; i < half; i++) {
        g->add_edge(i, i + half);
    }
    return g;
}

void plot_degree_distribution(const Graph &g,
                              const string &title,
                              const string &name, 
                              const string &color, 
                              bool legend, 
                              bool show) {
    namespace plt = matplotlibcpp;

    auto degree_distr = probability_distribution(g.get_degree_distribution());
    plt::named_loglog(name, degree_distr.first, degree_distr.second, color);
    plt::title(title);
    if (legend) plt::legend();
    if (show) plt::show();
}

// template <typename T>
// void plot(const std::vector<T> &x,
//           const std::vector<T> &y,
//           const std::string &name, 
//           const std::optional<std::string> &color, 
//           bool legend, 
//           bool show) {
//     namespace plt = matplotlibcpp;

//     if (color.has_value()) {
//         plt::named_plot(name, x, y, color.value());
//         if (legend) plt::legend();
//     } else {
//         plt::named_plot(name, x, y);
//     }
//     if (show) plt::show();
// }

bool starts_with(const string &s, const string &prefix) {
    return s.rfind(prefix, 0) != string::npos;
}

template <typename T>
T parse_line(const string &line, const function<T(const string&)> &parser) {
    auto idx = line.find('|');
    if (idx == string::npos) {  
        cerr << "Unable to parse line!" << endl;
        exit(123);
    }
    auto substr = line.substr(idx + 1);
    auto value  = parser(substr);
    return value;
}

tuple<string, int, float> parse_params(const string &s) {
    auto idx = s.find('[');
    if (idx == string::npos) {  
        cerr << "Unable to parse line!" << endl;
        exit(123);
    }
    auto name   = s;
    auto substr = s.substr(idx + 1);
    idx = substr.find(';');
    if (idx == string::npos) {  
        cerr << "Unable to parse line!" << endl;
        exit(123);
    }

    auto substr1 = substr.substr(0, idx);
    auto substr2 = substr.substr(idx);

    idx = substr1.find(':');
    auto iter = stoi(substr1.substr(idx + 1));

    idx = substr2.find(':');
    auto alpha = stof(substr2.substr(idx + 1));

    return make_tuple(name, iter, alpha);
}

int parse_int(const string &s) {
    return stoi(s.substr(s.find('|') + 1));
}

float parse_float(const string &s) {
    return stof(s.substr(s.find('|') + 1));
}

template <typename T>
function<T(const string&)> discard_after(char after, const function<T(const string&)> &parser) {
    return [after, parser](auto s) {
        auto idx = s.find(after);
        auto substr = s.substr(0, idx);
        return parser(substr);
    };
}

vector<GraphStatistics> load_statistics(const string &file, int starting_nodes) {
    auto stream = ifstream(file);
    if (!stream.is_open()) { return {}; }

    vector<GraphStatistics> all_statistics;

    string line;
    while (getline(stream, line)) {
        if (!starts_with(line, "graph")) continue;

        auto tuple = parse_params(line);
        getline(stream, line);
        auto nodes = discard_after<int>('%', parse_int)(line);
        getline(stream, line);
        auto edges = parse_int(line);
        getline(stream, line);
        auto k = discard_after<float>('(', parse_float)(line);
        getline(stream, line);
        auto density = parse_float(line);
        getline(stream, line);
        auto clustering = parse_float(line);
        getline(stream, line);
        auto r = parse_float(line);
        getline(stream, line);
        auto diameter = (int) parse_float(line);
        getline(stream, line);
        auto d = parse_float(line);
        getline(stream, line);
        auto lcc = parse_float(line);

        auto statistics = GraphStatistics(get<0>(tuple), 
                                          get<1>(tuple), 
                                          get<2>(tuple),
                                          starting_nodes,
                                          nodes, 
                                          edges, 
                                          k,
                                          density,
                                          clustering,
                                          r,
                                          diameter,
                                          d,
                                          lcc);
        all_statistics.push_back(statistics);
    }

    return all_statistics;
}

void benchmark(int nodes, int iter, float alpha, int seed) {
    // measure_execution_time<int>([nodes, iter, alpha, seed]() {
    //     expanding_shrinking_model(*perfect_matching_network(nodes), alpha, iter, seed, false);
    //     return 0;
    // });
}

shared_ptr<Graph> generate_es(int n, float alpha, int iter, int seed, bool verbose) {
    auto gr = perfect_matching_network(n);
    expanding_shrinking_model(*gr, alpha, iter, seed, verbose);
    return gr;
}

Graph generate_es_erdos_renyi(int n, int m, float alpha, int iter, int seed, bool verbose) {
    auto gr = Graph::generate_erdos_renyi(n, m, false, false, seed);
    expanding_shrinking_model(gr, alpha, iter, seed, verbose);
    return gr;
}

// void generate_statistics(int n, bool erdos_renyi, int m) {
//     for (float alpha = 0.1f; alpha <= 1.0f; alpha += 0.1f) {
//         cout << "Alpha: " << alpha << endl << endl;
//         for (int iter = 0; iter <= 100000; iter += 5000) {
//             if (erdos_renyi) {
//                 auto gr = generate_es_erdos_renyi(n, m, alpha, iter, iter);
//                 cout << endl;
//                 print_graph_statistics(*gr, "ExpandingShrinking [iter: "+to_string(iter)+"; alpha: "+to_string(alpha)+"]", true, true, true);
//             } else {
//                 auto gr = generate_es(n, alpha, iter, 123);
//                 cout << endl;
//                 print_graph_statistics(*gr, "ExpandingShrinking [iter: "+to_string(iter)+"; alpha: "+to_string(alpha)+"]", true, true, true);
//             }
//         }
//     }
// }

void generate_statistics(const std::string &file, 
                         int n, 
                         int m, 
                         float alpha, 
                         int start_iter, 
                         int max_iter, 
                         int iter_step,
                         bool erdos_renyi) {
    ofstream stream(file, ofstream::out | ofstream::app);
    if (!stream.is_open()) {
        cerr << "Unable to open file: " << file << endl;
        return;
    }

    cout << "Generating: " << endl
         << (erdos_renyi ? "G("+to_string(n)+","+to_string(m)+")" : "ES("+to_string(n)+")") << endl
         << "Alpha: " << alpha << ", Start: " << start_iter << ", Max: " << max_iter << ", Step: " << iter_step;

    for (int iter = start_iter; iter <= max_iter; iter += iter_step) {
        if (erdos_renyi) {
            auto gr = generate_es_erdos_renyi(n, m, alpha, iter, iter);
            cout << endl;
            print_graph_statistics(gr,
                                   "ExpandingShrinking [iter: " + to_string(iter) + "; alpha: " + to_string(alpha) + "]",
                                   false, true, true, true,
                                   stream);

        } else {
            auto gr = generate_es(n, alpha, iter, iter);
            cout << endl;
            print_graph_statistics(*gr,
                                   "ExpandingShrinking [iter: " + to_string(iter) + "; alpha: " + to_string(alpha) + "]",
                                   false, true, true, true,
                                   stream);
        }
    }
    stream.close();
}

void plot_degree_distributions(int n, int min_iter, int max_iter, int step) {
    for (float alpha = 0.0f; alpha <= 1.01f; alpha += 0.1f) {
        cout << "Alpha: " << alpha << endl << endl;
        int i = 0;
        for (int iter = min_iter; iter <= max_iter; iter += step) {
            auto gr = generate_es(n, alpha, iter, iter, true);
            if (gr->get_nodes_count() < 100) {
                cout << "Skipped graph: " << alpha << ", " << iter << endl;
                continue;
            } 
            plot_degree_distribution(*gr, to_string(alpha), to_string(iter), colors[i++], true, iter == max_iter);
        }
    }
}

void generate_degree_distributions(int n, int min_iter, int max_iter, int step, const std::string &file) {
    ofstream stream(file);
    if (!stream.is_open()) {
        cerr << "Unable to open file!" << endl;
        exit(12223);
    }
    for (float alpha = 0.0f; alpha <= 1.01f; alpha += 0.1f) {
        cout << "Alpha: " << alpha << endl << endl;
        int i = 0;
        for (int iter = min_iter; iter <= max_iter; iter += step) {
            stream << alpha << "," << iter << endl;
            auto gr = generate_es_erdos_renyi(n, n / 2, alpha, iter, iter, false);
            if (gr.get_nodes_count() < 100) {
                cout << "Skipped graph: " << alpha << ", " << iter << endl;
                continue;
            } 
            auto degree_distr = probability_distribution(gr.get_degree_distribution());
            for (auto it = degree_distr.first.cbegin(); it != degree_distr.first.cend(); it++) {
                stream << *it << " ";
            }
            stream << endl;
            for (auto it = degree_distr.second.cbegin(); it != degree_distr.second.cend(); it++) {
                stream << *it << " ";
            }
            stream << endl << endl;
        }
    }
    stream.close();
}

void plot_degree_distributions(const std::string &file) {
    ifstream stream(file);
    if (!stream.is_open()) {
        cerr << "Unable to open file!" << endl;
        exit(3211);
    }

    namespace plt = matplotlibcpp;

    auto current_alpha = -1.0f;

    string line;
    int i = 0;
    while (getline(stream, line)) {
        if (line.empty()) continue;

        auto idx   = line.find(',');
        auto alpha = stof(line.substr(0, idx));
        auto iter  = stoi(line.substr(idx + 1));

        if (current_alpha > 0.0f && current_alpha != alpha) {
            plt::show();
        }
        current_alpha = alpha;

        auto degrees = vector<float>();
        auto distr   = vector<float>();

        getline(stream, line);

        while ((idx = line.find(' ')) != string::npos) {
            if (line.substr(idx) == " ") break;
            auto val = stof(line.substr(0, idx));
            degrees.push_back(val);
            line = line.substr(idx + 1);
        }

        getline(stream, line);

        while ((idx = line.find(' ')) != string::npos) {
            if (line.substr(idx) == " ") break;
            auto val = stof(line.substr(0, idx));
            distr.push_back(val);
            line = line.substr(idx + 1);
        }

        plt::named_loglog(to_string(iter), degrees, distr, colors[i++]);
        plt::title("α: "+to_string(alpha));
        plt::legend();

        cout << iter << endl;
        
        if (iter == 200000) {
            plt::show();
            i = 0;
            current_alpha = -1.0f;
        }
    }
}

string exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

string exec(const string &cmd) {
    return exec(cmd.c_str());
}

double portrait_divergence(const std::string &edges_list_path1,
                           const std::string &edges_list_path2) {
    auto str = exec("python3 ../portrait_divergence/portrait_divergence.py --cpp " + edges_list_path1 + " " + edges_list_path2);
    return stod(str);
}


double portrait_divergence(const Graph &g1,
                           const std::string &edges_list_path2) {
    if (!g1.save_to_file_edge_list("tmp.edgelist")) {
        cerr << "Unable to save graph to temp. file!" << endl;
        exit(999);
    }
    return portrait_divergence("tmp.edgelist", edges_list_path2);
}

double portrait_divergence(const std::string &edges_list_path1,
                           const Graph &g2) {
    if (!g2.save_to_file_edge_list("tmp.edgelist")) {
        cerr << "Unable to save graph to temp. file!" << endl;
        exit(999);
    }
    return portrait_divergence(edges_list_path1, "tmp.edgelist");
}

double portrait_divergence(const Graph &g1,
                           const Graph &g2) {
    if (!g1.save_to_file_edge_list("tmp1.edgelist")) {
        cerr << "Unable to save graph to temp. file!" << endl;
        exit(999);
    }
    if (!g2.save_to_file_edge_list("tmp2.edgelist")) {
        cerr << "Unable to save graph to temp. file!" << endl;
        exit(999);
    }
    return portrait_divergence("tmp1.edgelist", "tmp2.edgelist");
}