#ifndef _PROGRESS_BAR_H_
#define _PROGRESS_BAR_H_

#include <iostream>
#include <iomanip>

template <typename T>
void progress_bar(T curr, T max) {
    std::cout << std::setprecision(1) << std::fixed;
    std::cout << curr << " / " << max << " [" << ((float) curr / max) * 100 << "%]" << "\r";
    flush(std::cout);
    std::cout << std::setprecision(5) << std::fixed;
}

#endif