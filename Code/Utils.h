#ifndef _UTILS_H_
#define _UTILS_H_

#include <iostream>
#include <Graph.h>
#include <functional>
#include <memory>

#include <GraphUtils.h>

#define WITHOUT_NUMPY
#include "matplotlibcpp.h"

template<typename T>
std::vector<T> filter(const std::vector<T> &vec, std::function<bool(const T&)> predicate) {
    auto filtered = std::vector<T>();
    copy_if(vec.begin(), vec.end(), 
            back_inserter(filtered), 
            predicate);
    return filtered;
}

template<typename T, typename U>
std::vector<U> fmap(const std::vector<T> &vec, std::function<U(const T&)> map_to) {
    auto mapped = std::vector<U>();
    transform(vec.begin(), vec.end(), 
              back_inserter(mapped), 
              map_to);
    return mapped;
}

void merge_nodes(Graph &g, unsigned a, unsigned b);

void split_node(Graph &g, unsigned a);

std::shared_ptr<Graph> perfect_matching_network(int nodes);

template <typename T>
void plot(const std::string &title, 
          const std::vector<T> &x,
          const std::vector<T> &y,
          const std::string &name, 
          const std::optional<std::string> &color, 
          bool legend, 
          bool show) {
    namespace plt = matplotlibcpp;

    if (color.has_value()) {
        plt::named_plot(name, x, y, color.value());
        if (legend) plt::legend();
    } else {
        plt::named_plot(name, x, y);
    }
    plt::title(title);
    if (show) plt::show();
}

template <typename T>
void plot(const std::string &title, 
          const std::vector<T> &x,
          const std::function<T(const T&)> &f,
          const std::string &name, 
          const std::optional<std::string> &color, 
          bool legend, 
          bool show) {
    auto y = fmap<T, T>(x, f);
    plot(title, x, y, name, color, legend, show);
}

bool starts_with(const std::string &s, const std::string &prefix);

struct GraphStatistics {
    std::string name;
    int iter;
    float alpha;
    int starting_nodes;
    
    int nodes;
    int edges;
    float k;
    float density;
    float clustering;
    float r;
    float diameter;
    float d;
    float lcc;

    GraphStatistics(const std::string &name, int iter, float alpha,
                    int starting_nodes, int nodes, int edges, float k,
                    float density, float clustering, float r, float diameter, float d,
                    float lcc)
        : iter(iter),
          alpha(alpha),
          starting_nodes(starting_nodes),
          name(name),
          nodes(nodes),
          edges(edges),
          k(k),
          density(density),
          clustering(clustering),
          r(r),
          diameter(diameter),
          d(d),
          lcc(lcc) {}

    GraphStatistics(const GraphStatistics &other)
        : iter(other.iter),
          alpha(other.alpha),
          starting_nodes(other.starting_nodes),
          name(other.name),
          nodes(other.nodes),
          edges(other.edges),
          k(other.k),
          density(other.density),
          clustering(other.clustering),
          r(other.r),
          diameter(other.diameter),
          d(other.d),
          lcc(other.lcc) {}
};

std::vector<GraphStatistics> load_statistics(const std::string &file, int starting_nodes);

static std::vector<std::string> colors = {
    "#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf",
    "#F0A3FF", "#0075DC", "#993F00", "#4C005C", "#191919", "#005C31", "#2BCE48", "#FFCC99", "#808080", "#94FFB5"
};

void benchmark(int nodes, int iter, float alpha, int seed);

std::shared_ptr<Graph> generate_es(int n, float alpha, int iter, int seed, bool verbose = true);
Graph generate_es_erdos_renyi(int n, int m, float alpha, int iter, int seed, bool verbose = true);

// void generate_statistics(int n, bool erdos_renyi, int m);

void generate_statistics(const std::string &file, 
                         int n, 
                         int m, 
                         float alpha, 
                         int start_iter, 
                         int max_iter, 
                         int iter_step,
                         bool erdos_renyi)  ;

template <typename T>
void plot_statistics(const std::vector<std::vector<GraphStatistics>> &grouped_statistics,
                     const std::string &title,
                     const std::function<T(const GraphStatistics&)> &extract_statistic) {
    int i = 0;
    for (auto it = grouped_statistics.cbegin(); it != grouped_statistics.cend(); it++) {
        auto x = fmap<GraphStatistics, T>(*it, [](auto &s) {
            return s.iter;
        });
        auto y = fmap<GraphStatistics, T>(*it, extract_statistic);
        plot<T>(title, x, y, "Alpha: " + std::to_string(it->front().alpha), colors[i++], true, it == grouped_statistics.cend() - 1);
    }
}

void plot_degree_distribution(const Graph &g,
                              const std::string &title,
                              const std::string &name,
                              const std::string &color,
                              bool legend = true,
                              bool show = true);
void plot_degree_distributions(int n, int min_iter, int max_iter, int step);
void plot_degree_distributions(const std::string &file);

void generate_degree_distributions(int n, int min_iter, int max_iter, int step, const std::string &file);

double portrait_divergence(const std::string &edges_list_path1,
                           const std::string &edges_list_path2);

double portrait_divergence(const Graph &g1,
                           const std::string &edges_list_path2);

double portrait_divergence(const std::string &edges_list_path1,
                           const Graph &g2);

double portrait_divergence(const Graph &g1,
                           const Graph &g2);

#endif /* _UTILS_H_ */