#ifndef _ES_MODEL_H_
#define _ES_MODEL_H_

#include "Utils.h"

/**
 * @brief Expanding-shrinking model.
 * 
 * - Uses war pact model for shrinking dynamics.
 * - Uses node splitting with two new nodes both inheriting all edges
 *   for expanding dynamics.
 * 
 * @param alpha The probability of shrinking the network at a given iteration.
 * @param iter Number of iterations.
 * @param seed Random generator seed.
 * @param select_nodes_for_merging A method for selecting two nodes for merging.
 * @param select_node_for_splitting A method for selecting a single node for splitting.
 * @param merge_nodes A method which merges the two nodes, thus modifying the underlying network.
 * @param split_node A method which splits the given node, thus modifying the underlying network.
 * @param size Returns a number of nodes in the network.
 * @param verbose Print progress.
 */
void expanding_shrinking_model(float alpha,
                               int iter,
                               int seed,
                               const std::function<std::pair<int, int>()> &select_nodes_for_merging,
                               const std::function<int()> &select_node_for_splitting,
                               const std::function<void(unsigned, unsigned)> &merge_nodes,
                               const std::function<void(unsigned)> &split_node,
                               bool verbose = true);

/**
 * @brief 'Default' ES model realisation using random node selection.
 * 
 * @param g Initial graph.
 * @param alpha The probability of shrinking the network at a given iteration.
 * @param iter Number of iterations.
 * @param seed Random number generator seed.
 * @param verbose Print progress.
 */
void expanding_shrinking_model(Graph &g, float alpha, int iter, int seed, bool verbose = true);

#endif /* _ES_MODEL_H_ */