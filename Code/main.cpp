#include <GraphUtils.cpp>
#include <igraph_wrapper.h>
#include <cstdlib>
#include <iostream>
#include <fstream>

#include "ESModel.h"
#include "arg_parser.h"
#include "ProgressBar.h"

using namespace std;

template <typename T>
T average(int n, const function<T()> &f) {
    T sum = 0.0;
    for (int i = 0; i < n; i++) {
        sum += f();
    }
    return sum / (T) n;
}

template <typename T, typename U>
pair<T, U> average(int n, const function<pair<T, U>()> &f) {
    T sum1 = 0.0;
    U sum2 = 0.0;
    for (int i = 0; i < n; i++) {
        auto r = f();
        sum1 += r.first;
        sum2 += r.second;
    }
    return make_pair(sum1 / (T) n, sum2 / (U) n);
}

template <typename T, typename U, typename V>
tuple<T, U, V> average(int n, const function<tuple<T, U, V>()> &f) {
    T sum1 = 0.0;
    U sum2 = 0.0;
    V sum3 = 0.0;
    for (int i = 0; i < n; i++) {
        auto r = f();
        sum1 += get<0>(r);
        sum2 += get<1>(r);
        sum3 += get<2>(r);
    }
    return make_tuple(sum1 / (T) n, sum2 / (U) n, sum3 / (V) n);
}

void generate_heatmap(int n, int iter, int repeat, const string &file) {
    auto g = [n, iter](float alpha, int seed) {
        return generate_es(n, alpha, iter, seed, false);
    };

    ofstream stream(file);
    if (!stream.is_open()) {
        cerr << "Unable to open file!" << endl;
        exit(999);
    }

    stream << "n: " << n << ", iter: " << iter << ", repeat: " << repeat << endl;

    // vector<vector<float>> all_portraits;
    // all_portraits.reserve(11);
    int i = 0;
    for (float alpha1 = 0.0f; alpha1 <= 1.001f; alpha1 += 0.1f) {
        // vector<float> portraits;
        // portraits.reserve(11);
        for (float alpha2 = 0.0f; alpha2 <= 1.001f; alpha2 += 0.1f) {
            auto p = average<float>(repeat, 
                                    [alpha1, alpha2, &g]() {
                                        return portrait_divergence(*g(alpha1, time(nullptr)),
                                                                   *g(alpha2, time(nullptr)));
                                    });
            stream << p << " ";
            progress_bar(++i, 11*11);
        }
        stream << endl;
    }
    stream.close();
}

void generate_heatmaps(const string &args) {
    generate_heatmap(10000, 9000, 5, "heatmap2.txt");
}

void plot_statistics(const string &file, int n) {
    auto statistics = load_statistics(file, n);

    vector<vector<GraphStatistics>> grouped_statistics;
    for (float alpha = 0.0f; alpha <= 1.0f; alpha += 0.1f) {
        auto stats = filter<GraphStatistics>(statistics, [alpha](auto &stat) {
            return abs(alpha - stat.alpha) <= 10e-5 && stat.nodes >= 100;
        });
        if (!stats.empty())
            grouped_statistics.push_back(stats);
    }

    plot_statistics<int>  (grouped_statistics, "n",       [](auto &s) { return s.nodes; });
    plot_statistics<int>  (grouped_statistics, "m",       [](auto &s) { return s.edges; });
    plot_statistics<float>(grouped_statistics, "LCC [%]", [](auto &s) { return s.lcc; });
    plot_statistics<float>(grouped_statistics, "<k>",     [](auto &s) { return s.k; });
    plot_statistics<float>(grouped_statistics, "density", [](auto &s) { return s.density; });
    plot_statistics<float>(grouped_statistics, "<C>",     [](auto &s) { return s.clustering; });
    plot_statistics<float>(grouped_statistics, "r",       [](auto &s) { return s.r; });
    plot_statistics<float>(grouped_statistics, "D",       [](auto &s) { return s.diameter; });
    plot_statistics<float>(grouped_statistics, "<d>",     [](auto &s) { return s.d; });
}

void generate_graph_statistics(const string &str) {
    auto n = parse_int(str, "n").value();
    auto alpha = 0.0f;
    {
        auto alpha_ = parse_double(str, "alpha");
        if (alpha_.has_value()) {
            alpha = (float) alpha_.value();
        }
    }
    auto iter = 0;
    {
        auto iter_ = parse_int(str, "iter");
        if (iter_.has_value()) {
            iter = iter_.value();
        }
    }
    auto max_iter = 100000;
    {
        auto iter_ = parse_int(str, "max_iter");
        if (iter_.has_value()) {
            iter = iter_.value();
        }
    }
    auto iter_step = 5000;
    {
        auto iter_ = parse_int(str, "iter_step");
        if (iter_.has_value()) {
            iter = iter_.value();
        }
    }
    auto erdos_renyi = false;
    auto m = 0;
    {
        auto er_ = parse_int(str, "erdos_renyi");
        if (er_.has_value()) {
            erdos_renyi = er_.value();
            m = parse_int(str, "m").value();
        }
    }
    auto suffix = string("");
    {
        auto suffix_ = parse_arg(str, "file_suffix");
        if (suffix_.has_value()) {
            suffix = suffix_.value();
        }
    }
    
    // for (float a = alpha; a <= 1.0f; a += 0.1f) {
        auto a = alpha;
        generate_statistics("results["+to_string(a)+"]"+suffix+".txt",
                            n,
                            m,
                            a, 
                            iter,
                            max_iter,
                            iter_step,
                            erdos_renyi);
    // }
}

struct ShrinkingDistances {
    int n;
    int iter;
    vector<float> alphas;
    vector<float> average_distances;
    vector<float> diameters;
};

void plot_shrinking_distances(const string &file) {
    ifstream stream(file);
    if (!stream.is_open()) { 
        cerr << "Unable to open the file!" << endl;
        exit(9912); 
    }

    vector<ShrinkingDistances> vec;

    auto parse_params = [](const string &str) {
        auto s    = str.substr(str.find(':') + 2);
        auto idx  = s.find(' ');
        auto n    = stoi(s.substr(0, idx));
        auto iter = stoi(s.substr(idx));
        return make_pair(n, iter);
    };

    string line;
    getline(stream, line);
    auto running = true;
    while (running) {
        auto params = parse_params(line);
        ShrinkingDistances sd;
        sd.n = params.first;
        sd.iter = params.second;
        getline(stream, line);
        do {
            auto idx = line.find(':');
            auto alpha = stof(line.substr(0, idx));
            line = line.substr(idx + 1);
            idx = line.find(',');
            auto distance = stof(line.substr(0, idx));
            line = line.substr(idx + 1);
            auto diameter = stof(line);

            sd.alphas.push_back(alpha);
            sd.average_distances.push_back(distance);
            sd.diameters.push_back(diameter);
            if (!getline(stream, line)) {
                running = false;
                break;
            }
        } while (!starts_with(line, "params:"));
        vec.push_back(sd);
    }
    stream.close();
    int i = 0;
    for (auto it = vec.cbegin(); it != vec.cend(); it++) {
        plot<float>("Shrinking distances", 
                    it->alphas, 
                    it->average_distances,
                    "["+to_string(it->iter)+"]", 
                    colors[i++], 
                    true,
                    it + 1 == vec.cend());
    }
    i = 0;
    for (auto it = vec.cbegin(); it != vec.cend(); it++) {
        plot<float>("Shrinking diameter", 
                    it->alphas, 
                    it->diameters,
                    "["+to_string(it->iter)+"]", 
                    colors[i++], 
                    true,
                    it + 1 == vec.cend());
    }
}

void plot_heatmap(const string &file) {
    ifstream stream(file);
    if (!stream.is_open()) {
        cerr << "Unable to open file!" << endl;
        exit(9191);
    }

    string first;
    getline(stream, first);
    string line;

    float *data = new float[11 * 11];
    int i = 0;
    
    while (getline(stream, line)) {
        for (int j = 0; j < 11; j++) {
            auto idx = line.find(' ');
            auto val = stof(line.substr(0, idx));
            data[i * 11 + j] = 1.0f - val;
            line = line.substr(idx + 1);
        }
        i++;
    }

    namespace plt = matplotlibcpp;
    auto keywords = map<string, string>();
    // keywords["cmap"] = "hot";
    // keywords["extent"] = "[0,1]";
    keywords["interpolation"] = "none";
    #ifndef WITHOUT_NUMPY
    plt::imshow(data, 11, 11, 1, keywords);
    // plt::colorbar();
    plt::show();
    #endif
    
    delete [] data;
}

void model_network(const Graph &network,
                //    const Graph &seed,
                   int repeat,
                   const string &output_file, 
                   bool append = false) {
    auto stream = append 
        ? ofstream(output_file, ofstream::out | ofstream::app) 
        : ofstream(output_file);
    if (!stream.is_open()) {
        cerr << "Unable to open file!" << endl;
        exit(98721);
    }

    auto n = network.get_nodes_count();
    auto m = network.get_edges_count();
    auto C = network.get_average_clustering_coefficient();

    stream << n << ", " << m << ", " << C << endl;

    auto f_time = [](float alpha, int I, int T) {
        return static_cast<int>((static_cast<float>(I - T)/(2.0f * alpha - 1.0f)));
    };

    for (float alpha = 0.0f; alpha <= 1.01f; alpha += 0.1f) {
        for (int initial_size = (float) n * 0.1f; initial_size <= n * 2; initial_size += (float) n * 0.1f) {
            auto time_ = f_time(alpha, initial_size, n);
            for (int t = (float) time_ * 0.8f; t <= (float) time_ * 1.2f; t += (float) time_ * 0.05f) {
                stream << alpha << ", " << initial_size << ", " << t << ": ";
                auto avg = average<float, float, int>(repeat, [initial_size, alpha, t, &network]() {
                    auto es = generate_es(initial_size + initial_size % 2, alpha, t, time(nullptr), false);
                    auto p = portrait_divergence(network, *es);
                    auto c = es->get_average_clustering_coefficient();
                    auto m = es->get_edges_count();
                    return make_tuple(p, c, m);
                });
                stream << get<0>(avg) << ", " << get<1>(avg) << ", " << get<2>(avg) << endl;
            }
            stream << endl;
        }
        stream << endl;
    }
}

int main(int argc, char **argv) {
    if (argc > 2) {
        auto str = to_string(argv, argc);
        // generate_graph_statistics(str);
        generate_heatmaps(str);
    }

    {
        // auto gr = Graph::generate_erdos_renyi(10, 5, false, false, 5);
        // merge_nodes(gr, 0, 3);
        // merge_nodes(gr, 0, 2);
        // split_node(gr, 0);
        // cout << endl;
        // auto g = perfect_matching_network(2);
        // merge_nodes(*g, 0, 1);
        // cout << endl;
    }


    {
        // generate_degree_distributions(50000, 0, 200000, 20000, "degree_distributionsER.txt");
        plot_degree_distributions("degree_distributionsER.txt");
        return 0;
    }

    // {
    //     auto g = *generate_es(20000, 0.5, 20000, 111);
    //     print_graph_statistics(g, "a", true, true, true);
    //     if (g.save_to_file_edge_list("ER.txt")) {
    //         g = Graph::read_edges_list("ER.txt", g.get_nodes_count(), g.get_edges_count(), false).value();
    //         cout << "Read" << endl;
    //         print_graph_statistics(g, "a", true, true, true);
    //     }
    //     return 0;
    // }

    // {
    //     vector<double> iters;
    //     vector<double> portraits;
    //     auto g = *generate_es(10000, 0.6, 10000, 11);
    //     for (int iter = 11000; iter <= 20000; iter += 1000) {
    //         auto portrait = portrait_divergence(g,
    //                                             *generate_es(10000, 0.5, iter, 11));
    //         iters.push_back(iter);
    //         portraits.push_back(portrait);
    //         // cout << "Portrait: " << portrait << endl;
    //     }
    //     plot<double>("Portrait", iters, portraits, "", "b", false, true);
    //     return 0;
    // }

    // {
    //     auto file = string("../data/imdb.net");
        
    //     auto trade = Graph::read_pajek(file).value();
 
    //     ofstream out("portrait_optimisation_imdb.txt", ofstream::out | ofstream::app);

    //     auto f2 = [](float alpha, int I, int n) {
    //         return n * (1.0f - 2.0f * alpha) + I;
    //     };

    //     for (float alpha = 0.0f; alpha <= 1.01f; alpha += 0.1f) {
    //         for (int iter = 0; iter < 200000; iter += 20000) {
    //             for (int size = 6000; size <= 60000; size += 6000) {
    //                 if (f2(alpha, size, iter) < 15000 || f2(alpha, size, iter) > 20000) { continue; }

    //                 out << alpha << ", " 
    //                     << iter << ", "
    //                     << size << ": ";
    //                 flush(out);

    //                 auto avg = average<float, float, int>(1, [size, alpha, iter, &trade]() {
    //                     auto es = generate_es(size + size % 2, alpha, iter, time(nullptr), false);
    //                     auto p  = portrait_divergence(trade, *es);
    //                     auto c  = es->get_average_clustering_coefficient();
    //                     auto m  = es->get_edges_count();
    //                     return make_tuple(p, c, m);
    //                 });
    //                 out << get<0>(avg) << ", " << get<1>(avg) << ", " << get<2>(avg) << endl;
    //             }
    //             out << endl;
    //         }
    //         out << endl;
    //     }
        
    //     return 0;
    // }

    {
        auto file = string("../data/trade.net");
        auto trade = Graph::read_pajek(file).value();

        vector<double> ps;
        int n = 20;
        for (int i = 0; i < n; i++) {
            // 0.9, 9500, 7700
            auto g = generate_es(7700, 0.9, 9500, time(nullptr), false);
            auto p = portrait_divergence(trade, *g);
            // auto p = g->get_avg_distance();

            if (isnan(p)) {
                print_graph_statistics(*g, "", true, true, true, true);
                cout << endl;
            }
            if (p == -1) continue;

            ps.push_back(p);
            progress_bar(i, n);
        }
        double sum = 0.0;
        for (auto it = ps.cbegin(); it != ps.cend(); it++) {
            sum += *it;
        }
        double mean = sum / (double) n;
        sum = 0.0;
        for (auto it = ps.cbegin(); it != ps.cend(); it++) {
            sum += pow(mean - *it, 2.0);
            cout << *it << ", ";
        }
        double std = sqrt(sum / (double) n);


        cout << endl << endl << "Mean: " << mean << ", std.: " << std << endl;
        return 0;
    }

    // plot_statistics("../results-ER.txt", 22000);
    // plot_statistics("../results-PM.txt", 22000);

    // {
        /// Distances

        // ofstream stream("distances.txt", ofstream::out | ofstream::app);
        // if (!stream.is_open()) { return 0; }
        // for (int iter = 1000; iter < 10000; iter += 500) {
        //     if (iter == 40000) { continue; }
        //     stream << "params: " << 5000 << " " << iter << endl;
        //     int i = 0;
        //     cout << iter << ": " << endl;
        //     for (float alpha = 0.0f; alpha <= 1.001f; alpha += 0.05) {
        //         auto g = generate_es(5000, alpha, iter, time(nullptr), false);
        //         stream << alpha << ": " << g->get_avg_distance() << ", " << g->get_effective_diameter_bfs((float) g->get_nodes_count() * 0.9f) << endl;
        //         progress_bar(++i, 20);
        //     }
        // }
        // stream.close();
        // plot_shrinking_distances("distances.txt");
        // return 0;
    // }

    auto f = [](float alpha, int I, int T) {
        return (int) (static_cast<float>(I - T)/(2.0f*alpha-1.0f));
    };

    auto f2 = [](float alpha, int I, int n) {
        return n * (1.0f - 2.0f * alpha) + I;
    };

    // auto g = generate_es(2000, 0.5f, 200000, 12221, true);
    // print_graph_statistics(*g, "", false, false, false, false, cout);
    
    // auto g = generate_es(22000, 0.6f, 100000, 12221);
    // cout << "Predicted: " << f2(0.6f, 22000, 100000) << endl;
    // print_graph_statistics(*g, "", false, false, false, false, cout);

    // vector<float> x;
    // for (int n = 0; n <= 100000; n += 1000) {
    //     x.push_back(n);
    // }
    // int i = 0;
    // for (float alpha = 0.0f; alpha <= 1.01f; alpha += 0.1f) {
    //     plot<float>("Predicted network sizes", x, [&f2, alpha](auto n) { return f2(alpha, 22000, n); }, to_string(alpha), colors[i], true, i == 10);
    //     progress_bar(++i, 11);
    // }

    // plot_heatmap("heatmap.txt");

    return 0;
} 